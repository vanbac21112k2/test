package vn.edu.iuh.fit.demo;

import java.io.File;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.google.common.base.Strings;

import vn.edu.iuh.fit.tool.DirExplorer;

public class report3 {
	public static void listMethodCalls(File projectDir) {
		new DirExplorer((level, path, file) -> path.endsWith(".java"), (level, 
				path, file) -> {
					System.out.println(path);
					System.out.println(Strings.repeat("=", path.length()));
					try {
						new VoidVisitorAdapter<Object>() {
							//implement
							@Override
							public void visit(PackageDeclaration n, Object arg) {
		                        super.visit(n, arg);
								String packageName = n.getNameAsString();
		                        if (!packageName.matches("com\\.companyname\\.[a-zA-Z_$][a-zA-Z\\d_$]*")) {
		                            // Kiểm tra xem package có theo đúng mẫu không
		                            System.err.println("Invalid package name: " + packageName);
		                        }
		                        System.out.println("Package:" + packageName);
							}
							@Override
							public void visit(FieldDeclaration n, Object arg) {
								super.visit(n, arg);
								System.out.println(" [F " + n.getBegin() + "] " + n);
							}
							@Override
							public void visit(MethodDeclaration n, Object arg) {
								super.visit(n, arg);
								System.out.println(" [L " + n.getBegin() + "]" + n.getDeclarationAsString());
							}
						}.visit(StaticJavaParser.parse(file), null);
					} catch (Exception e) {
						new RuntimeException(e);
					}
				}).explore(projectDir);
	}
	public static void main(String[] args) {
		File projectDir = new File("C:\\Users\\Student\\eclipse-workspace\\Lab2");
		listMethodCalls(projectDir);

	}

}
